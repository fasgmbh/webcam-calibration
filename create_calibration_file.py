import numpy as np
import cv2
import glob
import json
# This script extracts the intrinsic and extrinsic parameters of a camera
# from the analysis of a 7x10 chessboard image
#
# Resolution is important here, as the calibration parameters are given in
# pixels so parameters for one resolution are not directly appliable to another
#
# IMPORTANT: The defined area for the chessboard is the interior area, so for
# a 7x10 pattern, the size is 6x9

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*9,3), np.float32)
objp[:,:2] = np.mgrid[0:6,0:9].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

cameraName = "logitech910"
fname = "input/"+cameraName+".jpg"
img = cv2.imread(fname)
height, width = img.shape[:2]

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
cv2.imwrite("input/"+cameraName+"_gray.jpg",gray)
# Find the chess board corners
ret, corners = cv2.findChessboardCorners(gray, (6,9), None)

# If found, add object points, image points (after refining them)
if ret == True:
    objpoints.append(objp)

    corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
    imgpoints.append(corners2)

    # Draw and display the corners
    img = cv2.drawChessboardCorners(img, (6,9), corners2,ret)
    cv2.imshow('Chessboard detected',img)
    cv2.waitKey(0)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    outputPathFile = "output/"+cameraName+"_parameters"
    np.savez(outputPathFile, width=width, height=height, ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs, corners2=corners2)

    params = {
        "width": width,
        "height": height,
        "ret": ret,
        "MTX": mtx.tolist(),
        "dist": dist.tolist(),
        "rvecs": sum(rvecs[0].tolist(),[]),
        "tvecs": sum(tvecs[0].tolist(),[])
    }
    print params
    json.dump(params, open(outputPathFile+".json", "w"), indent=2)

    print "\n\nCamera calibration data has been recorded in Numpy format"
    print "in file '"+outputPathFile+".npz'"

    print "Also, a JSON file for human reading is available"
    print "in file '"+outputPathFile+".json'"
else:
    print "Corners are not found!"

cv2.destroyAllWindows()
